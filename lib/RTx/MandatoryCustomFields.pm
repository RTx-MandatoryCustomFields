package RTx::MandatoryCustomFields;

our $VERSION = "0.01";

1;

=head1 NAME

RTx::MandatoryCustomFields - RT Extension to require custom fields to be set before resolving a ticket

=head1 DESCRIPTION

This RT extension adds a field to custom field definitions, allowing 
the field to be designated as "Required".  If this is enabled, tickets 
with no value in that custom field will refuse to resolve.  Note that 
unlike the Mandatory Subject extension, the field does not have to be 
set to create the ticket.

Required fields are enforced in the web interface, not the RT API, so 
they can be bypassed fairly easily by command-line, email, or other 
mechanisms for changing a ticket's status.  This extension is for 
encouraging good habits, not for security.

=head1 INSTALLATION

 perl Makefile.PL
 make
 make initdb # if you have never installed this extension before
 make install

If your RT is not in the default path (/opt/rt3), you must set RTHOME 
first.

=head1 CONFIGURATION

Add this line to your RT_SiteConfig.pm:

Set(@Plugins, ( 'RTx::MandatoryCustomFields' ));

If you already have a Set(@Plugins ...) line, add 'RTx::MandatoryCustomFields'
to the list of values.

Then edit your custom field and check the 'Required' box.  Any number of 
custom fields may be flagged as required.

If the user tries to resolve a ticket when one or more required fields 
are empty, 

=head1 AUTHOR

Mark Wells <mark@freeside.biz>

=cut
